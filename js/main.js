// URL: base of BACKEND-API
var backendBase = "https://handy-api.cjd-rostock.de/api";

/**
 * @type {{backendUrl: string, get: AJAX.get, post: AJAX.post}}
 */
var AJAX = {
    backendUrl: "",
    /**
     * @param param [GET]
     * @param event to trigger on "load"
     */
    get: function (param, event) {

        var request = new XMLHttpRequest();
        request.open("GET", this.backendUrl + "?" + param.join("&"), true);
        request.setRequestHeader("Authorization", "Bearer " + sessionId);
        request.addEventListener("load", event);
        request.send();

    },
    /**
     * @param object to [POST] JSON-encoded
     * @param event to trigger on "load"
     */
    post: function (object, event) {

        var request = new XMLHttpRequest();
        request.open("POST", this.backendUrl, true);
        request.setRequestHeader("Authorization", "Bearer " + sessionId);
        request.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        request.addEventListener("load", event);
        request.send(JSON.stringify(object))

    }
};

/**
 * @param node, containing class as innerHTML
 */
function getStudents(node) {

    var classString = node.innerHTML;

    AJAX.backendUrl = backendBase + "/students";

    AJAX.get(["class=" + classString], function () {

        var returning = "";

        for (var i = 0; i < JSON.parse(this.responseText).data.length; i++) {

            returning += "<li class='list-group-item list-group-item-info' onclick='transmitStudent(this)' data-id="
                + JSON.parse(this.responseText).data[i].id
                + ">"
                + JSON.parse(this.responseText).data[i].name
                + "</li>";

        }

        document.getElementById("selectStudent").innerHTML = returning;
        document.getElementById("selectStudent").scrollIntoView();

    });

}

/**
 * @param node, containing student id as data-id
 */
function transmitStudent(node) {

    var studentId = parseInt(node.dataset.id);

    AJAX.backendUrl = backendBase + "/infringements";

    if (confirm(node.innerHTML + " melden?")) {

        AJAX.post({id: studentId}, function () {

            if (this.status === 401) {

                /*
                 proof for validation error
                 */
                loginInfoBox.className = "alert alert-danger";
                loginInfoBox.innerHTML = "Validationsfehler. <span class='link'  onclick='location.reload()'>neu laden</span>";
                localStorage.removeItem("sessionId");
                return;

            }

            alert("Schüler erfolgreich gemeldet");

        });

    }

}

function getClasses() {

    AJAX.backendUrl = backendBase + "/classes";

    AJAX.get([], function () {

        if (this.status === 401) {

            /*
             proof for validation error
             */
            loginInfoBox.className = "alert alert-danger";
            loginInfoBox.innerHTML = "Validationsfehler. <span class='link' onclick='location.reload()'>neu laden</span>";
            localStorage.removeItem("sessionId");
            return;

        }

        var returning = "";

        for (var i = 0; i < JSON.parse(this.responseText).data.length; i++) {

            returning += "<li class='list-group-item' onclick='getStudents(this)'>"
                + JSON.parse(this.responseText).data[i]
                + "</li>";

        }

        document.getElementById("selectClass").innerHTML = returning;

    });

}

/**
 * @param user
 * @param pass
 */
function login(user, pass) {

    AJAX.backendUrl = backendBase + "/login";

    AJAX.post({username: user, password: pass}, function () {

        var resp_json = JSON.parse(this.responseText);
        if (resp_json.token) {
            var token = resp_json.token;
            sessionId = token;
            if (document.getElementById("inputRemember").checked === true) {
                localStorage.setItem("sessionId", token);
            }
        }

        loginAction(this.status === 200)

    });

}

/**
 * @param bool, boolean [true or false] depending on login success
 */
function loginAction(bool) {

    if (bool) {

        document.getElementById("loginForm").style.display = "none";
        btnLogout.style.visibility = "visible";
        loginInfoBox.className = "alert alert-success";
        loginInfoBox.innerHTML = "Login erfolgreich!";
        getClasses();

    } else {

        btnLogout.style.visibility = "hidden";
        loginInfoBox.className = "alert alert-danger";
        loginInfoBox.innerHTML = "Login fehlgeschlagen.";

    }

}
